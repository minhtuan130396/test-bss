/****************************************************************************
** Meta object code from reading C++ file 'login.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "bss-hmi-qt/UI/login.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'login.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Login_t {
    QByteArrayData data[24];
    char stringdata0[458];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Login_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Login_t qt_meta_stringdata_Login = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Login"
QT_MOC_LITERAL(1, 6, 19), // "on_btnLogin_clicked"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 20), // "on_btnCancel_clicked"
QT_MOC_LITERAL(4, 48, 13), // "onRemoteLogin"
QT_MOC_LITERAL(5, 62, 11), // "const User*"
QT_MOC_LITERAL(6, 74, 4), // "user"
QT_MOC_LITERAL(7, 79, 14), // "onLoginTimeout"
QT_MOC_LITERAL(8, 94, 26), // "on_txtUsername_textChanged"
QT_MOC_LITERAL(9, 121, 4), // "arg1"
QT_MOC_LITERAL(10, 126, 26), // "on_txtPassword_textChanged"
QT_MOC_LITERAL(11, 153, 19), // "on_btnLogin_pressed"
QT_MOC_LITERAL(12, 173, 20), // "on_btnLogin_released"
QT_MOC_LITERAL(13, 194, 20), // "on_btnCancel_pressed"
QT_MOC_LITERAL(14, 215, 21), // "on_btnCancel_released"
QT_MOC_LITERAL(15, 237, 28), // "on_txtUsername_returnPressed"
QT_MOC_LITERAL(16, 266, 36), // "on_txtUsername_cursorPosition..."
QT_MOC_LITERAL(17, 303, 4), // "arg2"
QT_MOC_LITERAL(18, 308, 31), // "on_txtUsername_selectionChanged"
QT_MOC_LITERAL(19, 340, 25), // "on_txtUsername_textEdited"
QT_MOC_LITERAL(20, 366, 41), // "on_txtUsername_customContextM..."
QT_MOC_LITERAL(21, 408, 3), // "pos"
QT_MOC_LITERAL(22, 412, 36), // "on_txtUsername_windowIconText..."
QT_MOC_LITERAL(23, 449, 8) // "iconText"

    },
    "Login\0on_btnLogin_clicked\0\0"
    "on_btnCancel_clicked\0onRemoteLogin\0"
    "const User*\0user\0onLoginTimeout\0"
    "on_txtUsername_textChanged\0arg1\0"
    "on_txtPassword_textChanged\0"
    "on_btnLogin_pressed\0on_btnLogin_released\0"
    "on_btnCancel_pressed\0on_btnCancel_released\0"
    "on_txtUsername_returnPressed\0"
    "on_txtUsername_cursorPositionChanged\0"
    "arg2\0on_txtUsername_selectionChanged\0"
    "on_txtUsername_textEdited\0"
    "on_txtUsername_customContextMenuRequested\0"
    "pos\0on_txtUsername_windowIconTextChanged\0"
    "iconText"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Login[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x08 /* Private */,
       3,    0,   95,    2, 0x08 /* Private */,
       4,    1,   96,    2, 0x08 /* Private */,
       7,    0,   99,    2, 0x08 /* Private */,
       8,    1,  100,    2, 0x08 /* Private */,
      10,    1,  103,    2, 0x08 /* Private */,
      11,    0,  106,    2, 0x08 /* Private */,
      12,    0,  107,    2, 0x08 /* Private */,
      13,    0,  108,    2, 0x08 /* Private */,
      14,    0,  109,    2, 0x08 /* Private */,
      15,    0,  110,    2, 0x08 /* Private */,
      16,    2,  111,    2, 0x08 /* Private */,
      18,    0,  116,    2, 0x08 /* Private */,
      19,    1,  117,    2, 0x08 /* Private */,
      20,    1,  120,    2, 0x08 /* Private */,
      22,    1,  123,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    9,   17,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QPoint,   21,
    QMetaType::Void, QMetaType::QString,   23,

       0        // eod
};

void Login::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Login *_t = static_cast<Login *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btnLogin_clicked(); break;
        case 1: _t->on_btnCancel_clicked(); break;
        case 2: _t->onRemoteLogin((*reinterpret_cast< const User*(*)>(_a[1]))); break;
        case 3: _t->onLoginTimeout(); break;
        case 4: _t->on_txtUsername_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_txtPassword_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_btnLogin_pressed(); break;
        case 7: _t->on_btnLogin_released(); break;
        case 8: _t->on_btnCancel_pressed(); break;
        case 9: _t->on_btnCancel_released(); break;
        case 10: _t->on_txtUsername_returnPressed(); break;
        case 11: _t->on_txtUsername_cursorPositionChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 12: _t->on_txtUsername_selectionChanged(); break;
        case 13: _t->on_txtUsername_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: _t->on_txtUsername_customContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 15: _t->on_txtUsername_windowIconTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject Login::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Login.data,
      qt_meta_data_Login,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Login::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Login::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Login.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Login::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
