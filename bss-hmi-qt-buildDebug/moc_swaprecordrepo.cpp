/****************************************************************************
** Meta object code from reading C++ file 'swaprecordrepo.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "bss-hmi-qt/Data/Reposistory/SwapRecordRepo/swaprecordrepo.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'swaprecordrepo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SwapRecordRepo_t {
    QByteArrayData data[13];
    char stringdata0[143];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SwapRecordRepo_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SwapRecordRepo_t qt_meta_stringdata_SwapRecordRepo = {
    {
QT_MOC_LITERAL(0, 0, 14), // "SwapRecordRepo"
QT_MOC_LITERAL(1, 15, 18), // "onUpdateMqttServer"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 14), // "LocalSQLiteDb*"
QT_MOC_LITERAL(4, 50, 8), // "sqLiteDb"
QT_MOC_LITERAL(5, 59, 5), // "debug"
QT_MOC_LITERAL(6, 65, 10), // "updatetoDb"
QT_MOC_LITERAL(7, 76, 17), // "const SwapRecord*"
QT_MOC_LITERAL(8, 94, 10), // "swapRecord"
QT_MOC_LITERAL(9, 105, 18), // "updateToMqttServer"
QT_MOC_LITERAL(10, 124, 6), // "okSend"
QT_MOC_LITERAL(11, 131, 8), // "uint32_t"
QT_MOC_LITERAL(12, 140, 2) // "id"

    },
    "SwapRecordRepo\0onUpdateMqttServer\0\0"
    "LocalSQLiteDb*\0sqLiteDb\0debug\0updatetoDb\0"
    "const SwapRecord*\0swapRecord\0"
    "updateToMqttServer\0okSend\0uint32_t\0"
    "id"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SwapRecordRepo[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   42,    2, 0x0a /* Public */,
       6,    1,   43,    2, 0x0a /* Public */,
       9,    0,   46,    2, 0x0a /* Public */,
      10,    1,   47,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,

       0        // eod
};

void SwapRecordRepo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SwapRecordRepo *_t = static_cast<SwapRecordRepo *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onUpdateMqttServer((*reinterpret_cast< LocalSQLiteDb*(*)>(_a[1]))); break;
        case 1: _t->debug(); break;
        case 2: _t->updatetoDb((*reinterpret_cast< const SwapRecord*(*)>(_a[1]))); break;
        case 3: _t->updateToMqttServer(); break;
        case 4: _t->okSend((*reinterpret_cast< const uint32_t(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (SwapRecordRepo::*_t)(LocalSQLiteDb * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SwapRecordRepo::onUpdateMqttServer)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject SwapRecordRepo::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SwapRecordRepo.data,
      qt_meta_data_SwapRecordRepo,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SwapRecordRepo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SwapRecordRepo::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SwapRecordRepo.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int SwapRecordRepo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void SwapRecordRepo::onUpdateMqttServer(LocalSQLiteDb * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
