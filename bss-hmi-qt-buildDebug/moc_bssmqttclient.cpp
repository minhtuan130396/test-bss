/****************************************************************************
** Meta object code from reading C++ file 'bssmqttclient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "bss-hmi-qt/bssmqttclient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'bssmqttclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_BSSMqttClient_t {
    QByteArrayData data[36];
    char stringdata0[435];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BSSMqttClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BSSMqttClient_t qt_meta_stringdata_BSSMqttClient = {
    {
QT_MOC_LITERAL(0, 0, 13), // "BSSMqttClient"
QT_MOC_LITERAL(1, 14, 10), // "bssChanged"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 10), // "const BSS*"
QT_MOC_LITERAL(4, 37, 3), // "bss"
QT_MOC_LITERAL(5, 41, 9), // "bpChanged"
QT_MOC_LITERAL(6, 51, 9), // "const BP*"
QT_MOC_LITERAL(7, 61, 2), // "bp"
QT_MOC_LITERAL(8, 64, 11), // "onUserLogin"
QT_MOC_LITERAL(9, 76, 11), // "const User*"
QT_MOC_LITERAL(10, 88, 4), // "user"
QT_MOC_LITERAL(11, 93, 16), // "onDonePublicData"
QT_MOC_LITERAL(12, 110, 8), // "uint32_t"
QT_MOC_LITERAL(13, 119, 2), // "id"
QT_MOC_LITERAL(14, 122, 9), // "updateBSS"
QT_MOC_LITERAL(15, 132, 8), // "updateBp"
QT_MOC_LITERAL(16, 141, 16), // "updateSwapRecord"
QT_MOC_LITERAL(17, 158, 17), // "const SwapRecord*"
QT_MOC_LITERAL(18, 176, 10), // "swapRecord"
QT_MOC_LITERAL(19, 187, 5), // "start"
QT_MOC_LITERAL(20, 193, 21), // "syncLocaldbToServerdb"
QT_MOC_LITERAL(21, 215, 14), // "LocalSQLiteDb*"
QT_MOC_LITERAL(22, 230, 13), // "localSQLiteDb"
QT_MOC_LITERAL(23, 244, 16), // "mqttStateChanged"
QT_MOC_LITERAL(24, 261, 24), // "QMqttClient::ClientState"
QT_MOC_LITERAL(25, 286, 5), // "state"
QT_MOC_LITERAL(26, 292, 16), // "mqttErrorChanged"
QT_MOC_LITERAL(27, 309, 24), // "QMqttClient::ClientError"
QT_MOC_LITERAL(28, 334, 5), // "error"
QT_MOC_LITERAL(29, 340, 13), // "mqttConnected"
QT_MOC_LITERAL(30, 354, 22), // "checkingMessageReceive"
QT_MOC_LITERAL(31, 377, 12), // "QMqttMessage"
QT_MOC_LITERAL(32, 390, 3), // "msg"
QT_MOC_LITERAL(33, 394, 19), // "mqttMessageReceived"
QT_MOC_LITERAL(34, 414, 14), // "QMqttTopicName"
QT_MOC_LITERAL(35, 429, 5) // "topic"

    },
    "BSSMqttClient\0bssChanged\0\0const BSS*\0"
    "bss\0bpChanged\0const BP*\0bp\0onUserLogin\0"
    "const User*\0user\0onDonePublicData\0"
    "uint32_t\0id\0updateBSS\0updateBp\0"
    "updateSwapRecord\0const SwapRecord*\0"
    "swapRecord\0start\0syncLocaldbToServerdb\0"
    "LocalSQLiteDb*\0localSQLiteDb\0"
    "mqttStateChanged\0QMqttClient::ClientState\0"
    "state\0mqttErrorChanged\0QMqttClient::ClientError\0"
    "error\0mqttConnected\0checkingMessageReceive\0"
    "QMqttMessage\0msg\0mqttMessageReceived\0"
    "QMqttTopicName\0topic"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BSSMqttClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x06 /* Public */,
       5,    1,   87,    2, 0x06 /* Public */,
       8,    1,   90,    2, 0x06 /* Public */,
      11,    1,   93,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,   96,    2, 0x0a /* Public */,
      15,    1,   99,    2, 0x0a /* Public */,
      16,    1,  102,    2, 0x0a /* Public */,
      19,    0,  105,    2, 0x0a /* Public */,
      20,    1,  106,    2, 0x0a /* Public */,
      23,    1,  109,    2, 0x08 /* Private */,
      26,    1,  112,    2, 0x08 /* Private */,
      29,    0,  115,    2, 0x08 /* Private */,
      30,    1,  116,    2, 0x08 /* Private */,
      33,    2,  119,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 12,   13,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 21,   22,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 27,   28,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 31,   32,
    QMetaType::Void, QMetaType::QByteArray, 0x80000000 | 34,   32,   35,

       0        // eod
};

void BSSMqttClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BSSMqttClient *_t = static_cast<BSSMqttClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->bssChanged((*reinterpret_cast< const BSS*(*)>(_a[1]))); break;
        case 1: _t->bpChanged((*reinterpret_cast< const BP*(*)>(_a[1]))); break;
        case 2: _t->onUserLogin((*reinterpret_cast< const User*(*)>(_a[1]))); break;
        case 3: _t->onDonePublicData((*reinterpret_cast< const uint32_t(*)>(_a[1]))); break;
        case 4: _t->updateBSS((*reinterpret_cast< const BSS*(*)>(_a[1]))); break;
        case 5: _t->updateBp((*reinterpret_cast< const BP*(*)>(_a[1]))); break;
        case 6: _t->updateSwapRecord((*reinterpret_cast< const SwapRecord*(*)>(_a[1]))); break;
        case 7: _t->start(); break;
        case 8: _t->syncLocaldbToServerdb((*reinterpret_cast< LocalSQLiteDb*(*)>(_a[1]))); break;
        case 9: _t->mqttStateChanged((*reinterpret_cast< const QMqttClient::ClientState(*)>(_a[1]))); break;
        case 10: _t->mqttErrorChanged((*reinterpret_cast< const QMqttClient::ClientError(*)>(_a[1]))); break;
        case 11: _t->mqttConnected(); break;
        case 12: _t->checkingMessageReceive((*reinterpret_cast< QMqttMessage(*)>(_a[1]))); break;
        case 13: _t->mqttMessageReceived((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< const QMqttTopicName(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMqttClient::ClientState >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMqttClient::ClientError >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMqttMessage >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMqttTopicName >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (BSSMqttClient::*_t)(const BSS * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&BSSMqttClient::bssChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (BSSMqttClient::*_t)(const BP * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&BSSMqttClient::bpChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (BSSMqttClient::*_t)(const User * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&BSSMqttClient::onUserLogin)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (BSSMqttClient::*_t)(const uint32_t & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&BSSMqttClient::onDonePublicData)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject BSSMqttClient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_BSSMqttClient.data,
      qt_meta_data_BSSMqttClient,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *BSSMqttClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BSSMqttClient::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_BSSMqttClient.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int BSSMqttClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void BSSMqttClient::bssChanged(const BSS * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void BSSMqttClient::bpChanged(const BP * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void BSSMqttClient::onUserLogin(const User * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void BSSMqttClient::onDonePublicData(const uint32_t & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
