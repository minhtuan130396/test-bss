/****************************************************************************
** Meta object code from reading C++ file 'swapview.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "bss-hmi-qt/UI/swapview.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'swapview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SwapView_t {
    QByteArrayData data[15];
    char stringdata0[209];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SwapView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SwapView_t qt_meta_stringdata_SwapView = {
    {
QT_MOC_LITERAL(0, 0, 8), // "SwapView"
QT_MOC_LITERAL(1, 9, 17), // "onComStateChanged"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 9), // "COM_STATE"
QT_MOC_LITERAL(4, 38, 8), // "newState"
QT_MOC_LITERAL(5, 47, 9), // "onComFail"
QT_MOC_LITERAL(6, 57, 20), // "on_btnAction_clicked"
QT_MOC_LITERAL(7, 78, 17), // "finishSwapSession"
QT_MOC_LITERAL(8, 96, 18), // "guiUpdateSwapState"
QT_MOC_LITERAL(9, 115, 17), // "const SwapRecord*"
QT_MOC_LITERAL(10, 133, 10), // "swapRecord"
QT_MOC_LITERAL(11, 144, 16), // "on_btnNo_clicked"
QT_MOC_LITERAL(12, 161, 17), // "on_btnYes_clicked"
QT_MOC_LITERAL(13, 179, 13), // "animationSlot"
QT_MOC_LITERAL(14, 193, 15) // "onCloseSwapView"

    },
    "SwapView\0onComStateChanged\0\0COM_STATE\0"
    "newState\0onComFail\0on_btnAction_clicked\0"
    "finishSwapSession\0guiUpdateSwapState\0"
    "const SwapRecord*\0swapRecord\0"
    "on_btnNo_clicked\0on_btnYes_clicked\0"
    "animationSlot\0onCloseSwapView"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SwapView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x08 /* Private */,
       5,    0,   62,    2, 0x08 /* Private */,
       6,    0,   63,    2, 0x08 /* Private */,
       7,    0,   64,    2, 0x08 /* Private */,
       8,    1,   65,    2, 0x08 /* Private */,
      11,    0,   68,    2, 0x08 /* Private */,
      12,    0,   69,    2, 0x08 /* Private */,
      13,    0,   70,    2, 0x08 /* Private */,
      14,    0,   71,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SwapView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SwapView *_t = static_cast<SwapView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onComStateChanged((*reinterpret_cast< const COM_STATE(*)>(_a[1]))); break;
        case 1: _t->onComFail(); break;
        case 2: _t->on_btnAction_clicked(); break;
        case 3: _t->finishSwapSession(); break;
        case 4: _t->guiUpdateSwapState((*reinterpret_cast< const SwapRecord*(*)>(_a[1]))); break;
        case 5: _t->on_btnNo_clicked(); break;
        case 6: _t->on_btnYes_clicked(); break;
        case 7: _t->animationSlot(); break;
        case 8: _t->onCloseSwapView(); break;
        default: ;
        }
    }
}

const QMetaObject SwapView::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_SwapView.data,
      qt_meta_data_SwapView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SwapView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SwapView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SwapView.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int SwapView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
