/****************************************************************************
** Meta object code from reading C++ file 'cabinetcom.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "bss-hmi-qt/cabinetcom.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cabinetcom.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CabinetCom_t {
    QByteArrayData data[40];
    char stringdata0[534];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CabinetCom_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CabinetCom_t qt_meta_stringdata_CabinetCom = {
    {
QT_MOC_LITERAL(0, 0, 10), // "CabinetCom"
QT_MOC_LITERAL(1, 11, 16), // "onBSSLostConnect"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 12), // "onBSSChanged"
QT_MOC_LITERAL(4, 42, 10), // "const BSS*"
QT_MOC_LITERAL(5, 53, 3), // "bss"
QT_MOC_LITERAL(6, 57, 11), // "onBPChanged"
QT_MOC_LITERAL(7, 69, 9), // "const BP*"
QT_MOC_LITERAL(8, 79, 2), // "bp"
QT_MOC_LITERAL(9, 82, 2), // "sn"
QT_MOC_LITERAL(10, 85, 16), // "onCabinetChanged"
QT_MOC_LITERAL(11, 102, 14), // "const Cabinet*"
QT_MOC_LITERAL(12, 117, 3), // "cab"
QT_MOC_LITERAL(13, 121, 17), // "onComStateChanged"
QT_MOC_LITERAL(14, 139, 9), // "COM_STATE"
QT_MOC_LITERAL(15, 149, 5), // "state"
QT_MOC_LITERAL(16, 155, 20), // "onNewBatteryInserted"
QT_MOC_LITERAL(17, 176, 24), // "onBatteryIDAssignSuccess"
QT_MOC_LITERAL(18, 201, 20), // "onNewBatteryAccepted"
QT_MOC_LITERAL(19, 222, 15), // "onBatteryDetach"
QT_MOC_LITERAL(20, 238, 18), // "onBatteryCheckFail"
QT_MOC_LITERAL(21, 257, 16), // "onCabinetComFail"
QT_MOC_LITERAL(22, 274, 19), // "onSwapRecordChanged"
QT_MOC_LITERAL(23, 294, 17), // "const SwapRecord*"
QT_MOC_LITERAL(24, 312, 10), // "swapRecord"
QT_MOC_LITERAL(25, 323, 10), // "getComData"
QT_MOC_LITERAL(26, 334, 22), // "processBSSStateMachine"
QT_MOC_LITERAL(27, 357, 15), // "saveCabinetData"
QT_MOC_LITERAL(28, 373, 10), // "saveBPData"
QT_MOC_LITERAL(29, 384, 11), // "saveBSSData"
QT_MOC_LITERAL(30, 396, 20), // "startCabinetIDAssign"
QT_MOC_LITERAL(31, 417, 8), // "Cabinet*"
QT_MOC_LITERAL(32, 426, 11), // "startBPAuth"
QT_MOC_LITERAL(33, 438, 13), // "getTiltSensor"
QT_MOC_LITERAL(34, 452, 9), // "onComFail"
QT_MOC_LITERAL(35, 462, 9), // "configBSS"
QT_MOC_LITERAL(36, 472, 13), // "configCabinet"
QT_MOC_LITERAL(37, 486, 8), // "configBP"
QT_MOC_LITERAL(38, 495, 18), // "userSwappingCancel"
QT_MOC_LITERAL(39, 514, 19) // "userSwappingRequest"

    },
    "CabinetCom\0onBSSLostConnect\0\0onBSSChanged\0"
    "const BSS*\0bss\0onBPChanged\0const BP*\0"
    "bp\0sn\0onCabinetChanged\0const Cabinet*\0"
    "cab\0onComStateChanged\0COM_STATE\0state\0"
    "onNewBatteryInserted\0onBatteryIDAssignSuccess\0"
    "onNewBatteryAccepted\0onBatteryDetach\0"
    "onBatteryCheckFail\0onCabinetComFail\0"
    "onSwapRecordChanged\0const SwapRecord*\0"
    "swapRecord\0getComData\0processBSSStateMachine\0"
    "saveCabinetData\0saveBPData\0saveBSSData\0"
    "startCabinetIDAssign\0Cabinet*\0startBPAuth\0"
    "getTiltSensor\0onComFail\0configBSS\0"
    "configCabinet\0configBP\0userSwappingCancel\0"
    "userSwappingRequest"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CabinetCom[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  144,    2, 0x06 /* Public */,
       3,    1,  145,    2, 0x06 /* Public */,
       6,    2,  148,    2, 0x06 /* Public */,
      10,    1,  153,    2, 0x06 /* Public */,
      13,    1,  156,    2, 0x06 /* Public */,
      16,    1,  159,    2, 0x06 /* Public */,
      17,    1,  162,    2, 0x06 /* Public */,
      18,    1,  165,    2, 0x06 /* Public */,
      19,    1,  168,    2, 0x06 /* Public */,
      20,    1,  171,    2, 0x06 /* Public */,
      21,    0,  174,    2, 0x06 /* Public */,
      22,    1,  175,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      25,    0,  178,    2, 0x08 /* Private */,
      26,    0,  179,    2, 0x08 /* Private */,
      27,    1,  180,    2, 0x08 /* Private */,
      28,    1,  183,    2, 0x08 /* Private */,
      29,    1,  186,    2, 0x08 /* Private */,
      30,    1,  189,    2, 0x08 /* Private */,
      32,    1,  192,    2, 0x08 /* Private */,
      33,    1,  195,    2, 0x08 /* Private */,
      34,    0,  198,    2, 0x08 /* Private */,
      35,    1,  199,    2, 0x0a /* Public */,
      36,    1,  202,    2, 0x0a /* Public */,
      37,    1,  205,    2, 0x0a /* Public */,
      38,    0,  208,    2, 0x0a /* Public */,
      39,    0,  209,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 7, QMetaType::QString,    8,    9,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 23,   24,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 31,   12,
    QMetaType::Void, 0x80000000 | 31,   12,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CabinetCom::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CabinetCom *_t = static_cast<CabinetCom *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onBSSLostConnect(); break;
        case 1: _t->onBSSChanged((*reinterpret_cast< const BSS*(*)>(_a[1]))); break;
        case 2: _t->onBPChanged((*reinterpret_cast< const BP*(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 3: _t->onCabinetChanged((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 4: _t->onComStateChanged((*reinterpret_cast< const COM_STATE(*)>(_a[1]))); break;
        case 5: _t->onNewBatteryInserted((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 6: _t->onBatteryIDAssignSuccess((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 7: _t->onNewBatteryAccepted((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 8: _t->onBatteryDetach((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 9: _t->onBatteryCheckFail((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 10: _t->onCabinetComFail(); break;
        case 11: _t->onSwapRecordChanged((*reinterpret_cast< const SwapRecord*(*)>(_a[1]))); break;
        case 12: _t->getComData(); break;
        case 13: _t->processBSSStateMachine(); break;
        case 14: _t->saveCabinetData((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 15: _t->saveBPData((*reinterpret_cast< const BP*(*)>(_a[1]))); break;
        case 16: _t->saveBSSData((*reinterpret_cast< const BSS*(*)>(_a[1]))); break;
        case 17: _t->startCabinetIDAssign((*reinterpret_cast< Cabinet*(*)>(_a[1]))); break;
        case 18: _t->startBPAuth((*reinterpret_cast< Cabinet*(*)>(_a[1]))); break;
        case 19: _t->getTiltSensor((*reinterpret_cast< const BSS*(*)>(_a[1]))); break;
        case 20: _t->onComFail(); break;
        case 21: _t->configBSS((*reinterpret_cast< const BSS*(*)>(_a[1]))); break;
        case 22: _t->configCabinet((*reinterpret_cast< const Cabinet*(*)>(_a[1]))); break;
        case 23: _t->configBP((*reinterpret_cast< const BP*(*)>(_a[1]))); break;
        case 24: _t->userSwappingCancel(); break;
        case 25: _t->userSwappingRequest(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (CabinetCom::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onBSSLostConnect)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const BSS * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onBSSChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const BP * , const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onBPChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const Cabinet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onCabinetChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const COM_STATE & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onComStateChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const Cabinet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onNewBatteryInserted)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const Cabinet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onBatteryIDAssignSuccess)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const Cabinet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onNewBatteryAccepted)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const Cabinet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onBatteryDetach)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const Cabinet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onBatteryCheckFail)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onCabinetComFail)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (CabinetCom::*_t)(const SwapRecord * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CabinetCom::onSwapRecordChanged)) {
                *result = 11;
                return;
            }
        }
    }
}

const QMetaObject CabinetCom::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CabinetCom.data,
      qt_meta_data_CabinetCom,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CabinetCom::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CabinetCom::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CabinetCom.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int CabinetCom::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void CabinetCom::onBSSLostConnect()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void CabinetCom::onBSSChanged(const BSS * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CabinetCom::onBPChanged(const BP * _t1, const QString & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void CabinetCom::onCabinetChanged(const Cabinet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void CabinetCom::onComStateChanged(const COM_STATE & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void CabinetCom::onNewBatteryInserted(const Cabinet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void CabinetCom::onBatteryIDAssignSuccess(const Cabinet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void CabinetCom::onNewBatteryAccepted(const Cabinet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void CabinetCom::onBatteryDetach(const Cabinet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void CabinetCom::onBatteryCheckFail(const Cabinet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void CabinetCom::onCabinetComFail()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void CabinetCom::onSwapRecordChanged(const SwapRecord * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
