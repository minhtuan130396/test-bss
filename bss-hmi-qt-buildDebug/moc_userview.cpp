/****************************************************************************
** Meta object code from reading C++ file 'userview.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "bss-hmi-qt/UI/userview.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'userview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_UserView_t {
    QByteArrayData data[11];
    char stringdata0[191];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UserView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UserView_t qt_meta_stringdata_UserView = {
    {
QT_MOC_LITERAL(0, 0, 8), // "UserView"
QT_MOC_LITERAL(1, 9, 18), // "on_btnSwap_clicked"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 20), // "on_btnLogout_clicked"
QT_MOC_LITERAL(4, 50, 19), // "onUserActionTimeout"
QT_MOC_LITERAL(5, 70, 13), // "closeUserView"
QT_MOC_LITERAL(6, 84, 24), // "on_btnOnlyCharge_clicked"
QT_MOC_LITERAL(7, 109, 18), // "on_btnSwap_pressed"
QT_MOC_LITERAL(8, 128, 19), // "on_btnSwap_released"
QT_MOC_LITERAL(9, 148, 21), // "on_btnLogout_released"
QT_MOC_LITERAL(10, 170, 20) // "on_btnLogout_pressed"

    },
    "UserView\0on_btnSwap_clicked\0\0"
    "on_btnLogout_clicked\0onUserActionTimeout\0"
    "closeUserView\0on_btnOnlyCharge_clicked\0"
    "on_btnSwap_pressed\0on_btnSwap_released\0"
    "on_btnLogout_released\0on_btnLogout_pressed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UserView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    0,   61,    2, 0x08 /* Private */,
       5,    0,   62,    2, 0x08 /* Private */,
       6,    0,   63,    2, 0x08 /* Private */,
       7,    0,   64,    2, 0x08 /* Private */,
       8,    0,   65,    2, 0x08 /* Private */,
       9,    0,   66,    2, 0x08 /* Private */,
      10,    0,   67,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void UserView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UserView *_t = static_cast<UserView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btnSwap_clicked(); break;
        case 1: _t->on_btnLogout_clicked(); break;
        case 2: _t->onUserActionTimeout(); break;
        case 3: _t->closeUserView(); break;
        case 4: _t->on_btnOnlyCharge_clicked(); break;
        case 5: _t->on_btnSwap_pressed(); break;
        case 6: _t->on_btnSwap_released(); break;
        case 7: _t->on_btnLogout_released(); break;
        case 8: _t->on_btnLogout_pressed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject UserView::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_UserView.data,
      qt_meta_data_UserView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *UserView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UserView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_UserView.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int UserView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
