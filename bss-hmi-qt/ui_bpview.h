/********************************************************************************
** Form generated from reading UI file 'bpview.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BPVIEW_H
#define UI_BPVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BPView
{
public:
    QWidget *centralwidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QPushButton *btnSwap;
    QPushButton *btnCancel;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *labelSerialnumber;
    QLabel *lblBatSN;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout;
    QLabel *labelTemp;
    QLabel *lblSOH;
    QLabel *lblTemp;
    QLabel *labelSOH;
    QSpacerItem *verticalSpacer_2;
    QLabel *labelT;
    QLabel *lblCycles;
    QLabel *labelSOC;
    QLabel *labelCabinNum;

    void setupUi(QMainWindow *BPView)
    {
        if (BPView->objectName().isEmpty())
            BPView->setObjectName(QStringLiteral("BPView"));
        BPView->resize(800, 1280);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(BPView->sizePolicy().hasHeightForWidth());
        BPView->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QStringLiteral("UTM Hanzel"));
        BPView->setFont(font);
        BPView->setStyleSheet(QLatin1String("#centralwidget{\n"
"	border-image: url(:/UI/img/Custom Size - 29.png);\n"
"}\n"
""));
        centralwidget = new QWidget(BPView);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy1);
        centralwidget->setFont(font);
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(454, 600, 251, 361));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        btnSwap = new QPushButton(layoutWidget);
        btnSwap->setObjectName(QStringLiteral("btnSwap"));
        sizePolicy1.setHeightForWidth(btnSwap->sizePolicy().hasHeightForWidth());
        btnSwap->setSizePolicy(sizePolicy1);
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(16);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setUnderline(false);
        font1.setWeight(50);
        font1.setStrikeOut(false);
        btnSwap->setFont(font1);
        btnSwap->setCursor(QCursor(Qt::BlankCursor));
        btnSwap->setStyleSheet(QLatin1String("background: transparent;\n"
"background-image: url(:/UI/img/SELEX SBS icon/Group 371.png);\n"
"background-repeat: no-repeat;\n"
"text-decoration: none;\n"
"background-position:left top;\n"
""));
        btnSwap->setFlat(true);

        verticalLayout_2->addWidget(btnSwap);

        btnCancel = new QPushButton(layoutWidget);
        btnCancel->setObjectName(QStringLiteral("btnCancel"));
        sizePolicy1.setHeightForWidth(btnCancel->sizePolicy().hasHeightForWidth());
        btnCancel->setSizePolicy(sizePolicy1);
        btnCancel->setFont(font1);
        btnCancel->setCursor(QCursor(Qt::BlankCursor));
        btnCancel->setStyleSheet(QLatin1String("background: transparent;\n"
"background-image: url(:/UI/img/SELEX SBS icon/Group 370.png);\n"
"background-repeat: no-repeat;\n"
"text-decoration: none;\n"
"background-position:left top;\n"
""));
        btnCancel->setFlat(true);

        verticalLayout_2->addWidget(btnCancel);

        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(60, 600, 385, 361));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        labelSerialnumber = new QLabel(verticalLayoutWidget);
        labelSerialnumber->setObjectName(QStringLiteral("labelSerialnumber"));
        labelSerialnumber->setMinimumSize(QSize(216, 50));
        labelSerialnumber->setMaximumSize(QSize(500, 120));
        QFont font2;
        font2.setFamily(QStringLiteral("SFU Helvetica"));
        font2.setPointSize(20);
        font2.setBold(false);
        font2.setItalic(false);
        font2.setWeight(50);
        labelSerialnumber->setFont(font2);
        labelSerialnumber->setCursor(QCursor(Qt::BlankCursor));
        labelSerialnumber->setStyleSheet(QStringLiteral("color: rgb(141, 198, 255);"));
        labelSerialnumber->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(labelSerialnumber);

        lblBatSN = new QLabel(verticalLayoutWidget);
        lblBatSN->setObjectName(QStringLiteral("lblBatSN"));
        lblBatSN->setMinimumSize(QSize(159, 50));
        lblBatSN->setMaximumSize(QSize(16777215, 120));
        QFont font3;
        font3.setFamily(QStringLiteral("bill corp nar"));
        font3.setPointSize(21);
        font3.setBold(false);
        font3.setItalic(false);
        font3.setWeight(50);
        lblBatSN->setFont(font3);
        lblBatSN->setCursor(QCursor(Qt::BlankCursor));
        lblBatSN->setStyleSheet(QStringLiteral("color: rgb(32, 74, 135);"));
        lblBatSN->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lblBatSN);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        labelTemp = new QLabel(verticalLayoutWidget);
        labelTemp->setObjectName(QStringLiteral("labelTemp"));
        labelTemp->setMinimumSize(QSize(216, 50));
        labelTemp->setMaximumSize(QSize(16777215, 120));
        labelTemp->setFont(font2);
        labelTemp->setCursor(QCursor(Qt::BlankCursor));
        labelTemp->setStyleSheet(QStringLiteral("color: rgb(141, 198, 255);"));
        labelTemp->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(labelTemp, 0, 0, 1, 1);

        lblSOH = new QLabel(verticalLayoutWidget);
        lblSOH->setObjectName(QStringLiteral("lblSOH"));
        lblSOH->setMinimumSize(QSize(159, 50));
        lblSOH->setMaximumSize(QSize(16777215, 120));
        lblSOH->setFont(font3);
        lblSOH->setCursor(QCursor(Qt::BlankCursor));
        lblSOH->setStyleSheet(QStringLiteral("color: rgb(32, 74, 135)"));
        lblSOH->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(lblSOH, 2, 1, 1, 1);

        lblTemp = new QLabel(verticalLayoutWidget);
        lblTemp->setObjectName(QStringLiteral("lblTemp"));
        lblTemp->setEnabled(true);
        lblTemp->setMinimumSize(QSize(159, 50));
        lblTemp->setMaximumSize(QSize(16777215, 120));
        lblTemp->setFont(font3);
        lblTemp->setCursor(QCursor(Qt::BlankCursor));
        lblTemp->setStyleSheet(QStringLiteral("color: rgb(32, 74, 135)"));
        lblTemp->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(lblTemp, 2, 0, 1, 1);

        labelSOH = new QLabel(verticalLayoutWidget);
        labelSOH->setObjectName(QStringLiteral("labelSOH"));
        QFont font4;
        font4.setFamily(QStringLiteral("SFU Helvetica"));
        font4.setPointSize(18);
        labelSOH->setFont(font4);
        labelSOH->setStyleSheet(QStringLiteral("color: rgb(141, 198, 255);"));
        labelSOH->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(labelSOH, 0, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        labelT = new QLabel(verticalLayoutWidget);
        labelT->setObjectName(QStringLiteral("labelT"));
        labelT->setMinimumSize(QSize(216, 50));
        labelT->setMaximumSize(QSize(16777215, 120));
        labelT->setFont(font2);
        labelT->setCursor(QCursor(Qt::BlankCursor));
        labelT->setStyleSheet(QStringLiteral("color: rgb(141, 198, 255);"));
        labelT->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(labelT);

        lblCycles = new QLabel(verticalLayoutWidget);
        lblCycles->setObjectName(QStringLiteral("lblCycles"));
        lblCycles->setMinimumSize(QSize(159, 50));
        lblCycles->setMaximumSize(QSize(16777215, 120));
        lblCycles->setFont(font3);
        lblCycles->setCursor(QCursor(Qt::BlankCursor));
        lblCycles->setStyleSheet(QStringLiteral("color: rgb(32, 74, 135)"));
        lblCycles->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lblCycles);

        labelSOC = new QLabel(centralwidget);
        labelSOC->setObjectName(QStringLiteral("labelSOC"));
        labelSOC->setGeometry(QRect(450, 110, 251, 341));
        sizePolicy.setHeightForWidth(labelSOC->sizePolicy().hasHeightForWidth());
        labelSOC->setSizePolicy(sizePolicy);
        labelSOC->setMinimumSize(QSize(159, 50));
        labelSOC->setMaximumSize(QSize(16777215, 1677770));
        QFont font5;
        font5.setFamily(QStringLiteral("bill corp nar"));
        font5.setPointSize(29);
        font5.setBold(false);
        font5.setItalic(false);
        font5.setUnderline(false);
        font5.setWeight(50);
        font5.setStrikeOut(false);
        labelSOC->setFont(font5);
        labelSOC->setCursor(QCursor(Qt::BlankCursor));
        labelSOC->setStyleSheet(QLatin1String("color: rgb(141, 198, 255);\n"
"background: transparent;\n"
"background-image:url(:/img/UI/images/SELEX SBS icon/Group -1.png);\n"
"background-repeat: no-repeat;\n"
"text-decoration: none;"));
        labelSOC->setAlignment(Qt::AlignCenter);
        labelCabinNum = new QLabel(centralwidget);
        labelCabinNum->setObjectName(QStringLiteral("labelCabinNum"));
        labelCabinNum->setGeometry(QRect(110, 260, 111, 91));
        QFont font6;
        font6.setFamily(QStringLiteral("UTM BryantLG"));
        font6.setPointSize(67);
        labelCabinNum->setFont(font6);
        labelCabinNum->setStyleSheet(QStringLiteral("color: rgb(32, 74, 135);"));
        labelCabinNum->setAlignment(Qt::AlignCenter);
        BPView->setCentralWidget(centralwidget);

        retranslateUi(BPView);

        QMetaObject::connectSlotsByName(BPView);
    } // setupUi

    void retranslateUi(QMainWindow *BPView)
    {
        BPView->setWindowTitle(QApplication::translate("BPView", "MainWindow", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        BPView->setToolTip(QApplication::translate("BPView", "<html><head/><body><p><br/></p></body></html>", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        btnSwap->setText(QString());
        btnCancel->setText(QString());
        labelSerialnumber->setText(QApplication::translate("BPView", "S\341\273\221 Seri", Q_NULLPTR));
        lblBatSN->setText(QApplication::translate("BPView", "BP-125487", Q_NULLPTR));
        labelTemp->setText(QApplication::translate("BPView", "Nhi\341\273\207t \304\220\341\273\231", Q_NULLPTR));
        lblSOH->setText(QApplication::translate("BPView", "90%", Q_NULLPTR));
        lblTemp->setText(QApplication::translate("BPView", "30\302\260C", Q_NULLPTR));
        labelSOH->setText(QApplication::translate("BPView", "SOH", Q_NULLPTR));
        labelT->setText(QApplication::translate("BPView", "S\341\273\221 Chu K\341\273\263", Q_NULLPTR));
        lblCycles->setText(QApplication::translate("BPView", "200", Q_NULLPTR));
        labelSOC->setText(QApplication::translate("BPView", "20%", Q_NULLPTR));
        labelCabinNum->setText(QApplication::translate("BPView", "10", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class BPView: public Ui_BPView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BPVIEW_H
