#include "user.h"

User::User()
{

}

QString User::getUsername() const
{
    return username;
}

void User::setUsername(const QString &value)
{
    username = value;
}

QString User::getPassword() const
{
    return password;
}

void User::setPassword(const QString &value)
{
    password = value;
}

void User::toJson(QJsonObject &json) const
{
    json["user_name"]= username;
    json["password"]= password;
}

void User::fromJson(const QJsonObject &json)
{
   if (json.contains("user") && json["user"].isString()){
        username = json["user"].toString();
   }

   if (json.contains("password") && json["password"].isString()){
        password = json["password"].toString();
   }
}

int32_t User::parse(const QByteArray &data, const char sep)
{
    (void)data;
    (void)sep;
   return -1;
}

