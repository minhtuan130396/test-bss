#ifndef USER_H
#define USER_H
#include <QString>
#include "basemodel.h"

class User:public BaseModel
{
private:
    QString username;
    QString password;
public:
    explicit User();

    QString getUsername() const;
    void setUsername(const QString &value);

    QString getPassword() const;
    void setPassword(const QString &value);


    // BaseModel interface
public:
    void toJson(QJsonObject &json) const;
    void fromJson(const QJsonObject &json);
    int32_t parse(const QByteArray &data, const char sep);
};

#endif // USER_H
