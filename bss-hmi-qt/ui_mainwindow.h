/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *btnLogin;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(800, 1280);
        MainWindow->setMinimumSize(QSize(600, 1024));
        QFont font;
        font.setFamily(QStringLiteral("UTM Hanzel"));
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/UI/images/bp.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QStringLiteral(""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        centralwidget->setStyleSheet(QStringLiteral(""));
        btnLogin = new QPushButton(centralwidget);
        btnLogin->setObjectName(QStringLiteral("btnLogin"));
        btnLogin->setGeometry(QRect(0, -31, 800, 1311));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnLogin->sizePolicy().hasHeightForWidth());
        btnLogin->setSizePolicy(sizePolicy);
        btnLogin->setMinimumSize(QSize(566, 64));
        QFont font1;
        font1.setFamily(QStringLiteral("SFU Helvetica"));
        font1.setPointSize(20);
        font1.setItalic(false);
        btnLogin->setFont(font1);
        btnLogin->setCursor(QCursor(Qt::BlankCursor));
        btnLogin->setAutoFillBackground(false);
        btnLogin->setStyleSheet(QLatin1String("QPushButton {\n"
"	boder:none;\n"
"\n"
"    background-image:url(:/UI/img/mainview.png);\n"
"    background-repeat: no-repeat;\n"
"\n"
"}\n"
"QPushButton:pressed {\n"
"	boder:none;\n"
"    background-image:url(:/UI/img/mainview2.png);\n"
"    background-repeat: no-repeat;\n"
"}\n"
"	color: rgb(32, 74, 135);\n"
"\n"
""));
        btnLogin->setIconSize(QSize(200, 200));
        btnLogin->setAutoRepeat(false);
        btnLogin->setAutoRepeatDelay(300);
        btnLogin->setAutoDefault(false);
        btnLogin->setFlat(false);
        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        btnLogin->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "BSS", Q_NULLPTR));
        btnLogin->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        btnLogin->setShortcut(QString());
#endif // QT_NO_SHORTCUT
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
