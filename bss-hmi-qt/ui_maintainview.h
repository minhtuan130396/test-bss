/********************************************************************************
** Form generated from reading UI file 'maintainview.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINTAINVIEW_H
#define UI_MAINTAINVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MaintainView
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MaintainView)
    {
        if (MaintainView->objectName().isEmpty())
            MaintainView->setObjectName(QStringLiteral("MaintainView"));
        MaintainView->resize(609, 649);
        centralwidget = new QWidget(MaintainView);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        MaintainView->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MaintainView);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 609, 24));
        MaintainView->setMenuBar(menubar);
        statusbar = new QStatusBar(MaintainView);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MaintainView->setStatusBar(statusbar);

        retranslateUi(MaintainView);

        QMetaObject::connectSlotsByName(MaintainView);
    } // setupUi

    void retranslateUi(QMainWindow *MaintainView)
    {
        MaintainView->setWindowTitle(QApplication::translate("MaintainView", "MainWindow", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MaintainView: public Ui_MaintainView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINTAINVIEW_H
