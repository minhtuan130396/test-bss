#include "userview.h"
#include "ui_userview.h"
#include "UI/swapview.h"
#include "bpview.h"

User *UserView::getUserModel() const
{
    return userModel;
}

UserView::UserView(const User* user,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UserView)
{
    ui->setupUi(this);
    CabinetCom* cab = CabinetCom::getCabCom();
    this->cabCom = cab;
//    if(cab->getBssModel()->getCabinets().at(5)->getOpState()== CAB_OP_ST_EMPTY){
//        this->ui->btnOnlyCharge->setText("Xạc tại chỗ");
//    }
//    else{
//        this->ui->btnOnlyCharge->setText("Nhận lại pin");
//    }
    this->setAttribute( Qt::WA_DeleteOnClose, true);
    userModel=new User();
    userModel->setUsername(user->getUsername());
    userModel->setPassword(user->getPassword());
    this->ui->lblUserName->setText(user->getUsername());

    userActionTimeout=new QTimer();
    connect(this->cabCom, &CabinetCom::onBSSLostConnect, this, &UserView::closeUserView);
    connect(userActionTimeout,&QTimer::timeout,this,&UserView::onUserActionTimeout);
    connect(this, &UserView::destroyed, userActionTimeout , &QTimer::deleteLater);
    userActionTimeout->start(10000);
}

UserView::~UserView()
{
    delete ui;
}

void UserView::on_btnSwap_clicked()
{
#if 1
    CabinetCom* cab = CabinetCom::getCabCom();
    cab->getSwapRecord()->setState(SWAP_RECORD_ST_SELECT_EMPTY_CAB);
    SwapView* swapView = new SwapView(cab);
    swapView->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    swapView->showFullScreen();
    closeUserView();
#endif
#if 0
    BPView* bpView = new BPView(new BP());
    bpView->show();
    close();
#endif
}

void UserView::on_btnLogout_clicked()
{
    closeUserView();
}

//void UserView::on_btnCheckBattery_clicked()
//{
//    CabinetCom* cab= CabinetCom::getCabCom();
//    cab->getSwapRecord()->setState(SWAP_RECORD_ST_CHARGE_ONLY);
//    SwapView* swapView = new SwapView(cab);
//    swapView->setWindowFlags(Qt::Window | Qt::FramelessWindowHint) ;
//    swapView->showFullScreen();
//    closeUserView();
//}

void UserView::onUserActionTimeout()
{
    closeUserView();
}

void UserView::closeUserView()
{
   userActionTimeout->stop();
   close();
}

void UserView::on_btnOnlyCharge_clicked()
{
//    CabinetCom* cab= CabinetCom::getCabCom();
//    cab->getSwapRecord()->setState(SWAP_RECORD_ST_CHARGE_ONLY);
//    SwapView* swapView = new SwapView(cab);
//    swapView->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
//    swapView->showFullScreen();
//    closeUserView();
}


void UserView::on_btnSwap_pressed()
{
    this->ui->btnSwap->setStyleSheet("background: transparent;"
                                     "background-image:url(:/UI/img/SELEX SBS icon/Group 372.png);"
                                     "background-repeat: no-repeat;"
                                     "background-position:left top;"
                                     "background-position:center;");
}



void UserView::on_btnSwap_released()
{
    this->ui->btnSwap->setStyleSheet("background: transparent;"
                                     "background-image:url(:/UI/img/SELEX SBS icon/Group 371.png);"
                                     "background-repeat: no-repeat;"
                                     "background-position:left top;"
                                     "background-position:center;");
}

void UserView::on_btnLogout_released()
{
    this->ui->btnLogout->setStyleSheet("background: transparent;"
                                       "background-image:url(:/UI/img/SELEX SBS icon/Group 370.png);"
                                       "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}

void UserView::on_btnLogout_pressed()
{
    this->ui->btnLogout->setStyleSheet("background: transparent;"
                                       "background-image:url(:/UI/img/SELEX SBS icon/Group 373.png);"
                                       "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}
