#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "UI/swapview.h"
#include "UI/login.h"
#include <QPainter>

MainWindow::MainWindow(CabinetCom* cabCom,QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->cabCom=cabCom;
    connect(this->cabCom,&CabinetCom::onBSSChanged,this,&MainWindow::onBSSStateChanged);
    connect(this->cabCom,&CabinetCom::onBSSChanged,this, &MainWindow::onBSSTiltChange);
    //this->ui->lblTitle->setText("Selex Battery Swapping System");

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::onBSSTiltChange(const BSS *bss){
//    if(bss->getTiltState()!=TILT_ST_ACTIVE) {
//        this->ui->btnLogin->setText("");
//        this->ui->btnLogin->setEnabled(true);
//        return;
//    }
//    qDebug() <<"has tilt";
//    this->ui->btnLogin->setText("Trạm nghiêng");
//    this->ui->btnLogin->setEnabled(false);
}
void MainWindow::onBSSStateChanged(const BSS* bss){

    switch (bss->getState()) {
        case BSS_ST_MAINTAIN:
        this->ui->btnLogin->setText("Hệ thống đang bảo trì");
        this->ui->btnLogin->setStyleSheet("border-image: url(:/UI/img/mainview.png);}"
                                          "pressed {background-image:url(:/img/UI/images/mainview2.png);"
                                          "background-repeat: no-repeat;}");
        this->ui->btnLogin->setEnabled(true);
        break;
    case BSS_ST_SWAP:
        this->ui->btnLogin->setText("");
        this->ui->btnLogin->setEnabled(true);
        break;
    case BSS_ST_SYNC:
        this->ui->btnLogin->setText("");
        this->ui->btnLogin->setEnabled(true);
        break;
    case BSS_ST_FAIL:
        this->ui->btnLogin->setText("Hệ thống lỗi");
        this->ui->btnLogin->setStyleSheet("border-image: url(:/UI/img/mainview.png);");
        this->ui->btnLogin->setEnabled(true);
        break;
    case BSS_ST_INIT:
        this->ui->btnLogin->setText("Đang khởi tạo");
        this->ui->btnLogin->setStyleSheet("border-image: url(:/UI/img/mainview.png);");
        this->ui->btnLogin->setEnabled(true);
        break;
    default:
        break;
    }
}
void MainWindow::on_btnLogin_clicked()
{
    Login* loginView=new Login(BSSMqttClient::getBSSMqttClient());
    loginView->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    loginView->showFullScreen();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
        QPainter painter(this);
        painter.setRenderHint(QPainter::HighQualityAntialiasing);
        painter.drawPixmap(0, 0, QPixmap("/home/pi/Workspace/bss-hmi-qt/UI/images/V2/view_welcome.png").scaled(size()));

        QWidget::paintEvent(event);
}

void MainWindow::on_btnLogin_pressed()
{
    this->ui->btnLogin->setStyleSheet("background-image:url(:/UI/img/mainview1.png);"
                                      "background-repeat: no-repeat;");
}


void MainWindow::on_btnLogin_released()
{
    this->ui->btnLogin->setStyleSheet("background-image:url(:/UI/img/mainview.png);"
                                      "background-repeat: no-repeat;");
}

