#include "login.h"
#include "ui_login.h"
#include "UI/swapview.h"
#include "userview.h"
#include <QPainter>
#include <QList>
#include <QVector>
#include <QStringList>
uint8_t is_staff_login = 0;
uint8_t cab_id = 0;
QVector <QStringList> listDataUserLazadaHub = {
    {"Test", "0"},
    {"Giang", "1234"},
    {"Quang", "1234"},
    {"User3", "pass3"},
    {"User4", "pass4"},
    {"User5", "pass5"},
    {"User6", "pass6"},
};
Login::Login(BSSMqttClient* mqttClient, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    this->setAttribute( Qt::WA_DeleteOnClose, true );
    this->ui->lblLoginStatus->setText("");
    this->mqttClient=mqttClient;
    this->cabCom = CabinetCom::getCabCom();
    connect(this->cabCom, &CabinetCom::onBSSLostConnect,this, &Login::on_btnCancel_clicked);
    connect(this->mqttClient,&BSSMqttClient::onUserLogin,this,&Login::onRemoteLogin);
    loginTimeoutTimer=new QTimer(this);
    connect(loginTimeoutTimer,&QTimer::timeout,this,&Login::onLoginTimeout);
    connect(this, &Login::destroyed, loginTimeoutTimer , &QTimer::deleteLater);
    loginTimeoutTimer->start(60000);  //10000
    ui->btnLogin->setFocus();
}

Login::~Login()
{
    delete ui;
}

void Login::on_btnLogin_clicked()
{
    QString username=ui->txtUsername->text();
    QString password=ui->txtPassword->text();
    for (int i=0; i< listDataUserLazadaHub.length(); i++)
    {
        if(((username == listDataUserLazadaHub[i][0]) && (password == listDataUserLazadaHub[i][1])) || (username=="Test" && password.toInt()>=0 && password.toInt() < 15)){
            if(username=="Test"){
                is_staff_login = 1;
                cab_id = password.toUInt()-1;
            }
            this->ui->lblLoginStatus->setText("");
            User* user=new User();
            user->setUsername(username);
            CabinetCom::getCabCom()->getSwapRecord()->setUser(username);
            UserView* userView=new UserView(user);
            userView->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
            userView->showFullScreen();
            close();
            return;
        }
    }
        this->ui->txtUsername->setFocus();
        this->ui->lblLoginStatus->setVisible(true);
        this->ui->lblLoginStatus->setText("Sai mật khẩu hoặc tài khoản");
}

void Login::on_btnCancel_clicked()
{
    loginTimeoutTimer->stop();
    close();
}

void Login::onRemoteLogin(const User *user)
{
    qDebug()<<"START";
    UserView* userView=new UserView(user);
    userView->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    userView->showFullScreen();
    loginTimeoutTimer->start();

    close();

}

void Login::onLoginTimeout()
{
    loginTimeoutTimer->stop();
    close();
}

void Login::on_txtUsername_textChanged(const QString &arg1)
{
    (void)arg1;
    loginTimeoutTimer->stop();
    loginTimeoutTimer->start(10000);
}

void Login::on_txtPassword_textChanged(const QString &arg1)
{
    (void)arg1;
   loginTimeoutTimer->stop();
   loginTimeoutTimer->start(10000);
}

void Login::on_btnLogin_pressed()
{
    this->ui->btnLogin->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 365.png);"
                                      "background-repeat: no-repeat;"
                                      "background-position:left top;"
                                      "background-position:center;");
}

void Login::on_btnLogin_released()
{
    this->ui->btnLogin->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 368.png);"
                                      "background-repeat: no-repeat;"
                                      "background-position:left top;"
                                      "background-position:center;");
}

void Login::on_btnCancel_pressed()
{
    this->ui->btnCancel->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 366.png);"
                                      "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}

void Login::on_btnCancel_released()
{
    this->ui->btnCancel->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 367.png);"
                                      "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}

void Login::on_txtUsername_returnPressed()
{

}

void Login::on_txtUsername_cursorPositionChanged(int arg1, int arg2)
{
    //has text inside changed
}

void Login::on_txtUsername_selectionChanged()
{
    qDebug()<<"HASSSSSSSSSSSSSSSSSSSSSSSSSSSSS";

}

void Login::on_txtUsername_textEdited(const QString &arg1)
{
    qDebug()<<"textedited";
}

void Login::on_txtUsername_customContextMenuRequested(const QPoint &pos)
{
    qDebug()<<"custom";
}

void Login::on_txtUsername_windowIconTextChanged(const QString &iconText)
{
    qDebug()<<"ICON CHANGE";
}
