#include "bpview.h"
#include "ui_bpview.h"
#include "Data/Model/bp.h"
#include "UI/swapview.h"
BPView::BPView(const Cabinet* cab,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BPView)
{
    this->cabCom = CabinetCom::getCabCom();
    ui->setupUi(this);
    this->cab=(Cabinet*)cab;
    userActionTimeout=new QTimer(this);
    connect(this->cabCom,&CabinetCom::onBSSLostConnect,this,&BPView::closeBPView);
    connect(userActionTimeout,&QTimer::timeout,this,&BPView::onUserActionTimeout);
    connect(this, &BPView::destroyed, userActionTimeout , &QTimer::deleteLater);
    userActionTimeout->start(10000);
    showBPData();
}

BPView::~BPView()
{
    delete ui;
}

void BPView::on_btnSwap_clicked()
{
#if 1
   SwapView* swapView=(SwapView*)this->parent();
   swapView->onUserSwapRequest();
   closeBPView();
#endif
}

void BPView::on_btnCancel_clicked()
{
   SwapView* swapView=(SwapView*)this->parent();
   swapView->onUserSwapCancel();
   closeBPView();
}

void BPView::onUserActionTimeout()
{
   SwapView* swapView=(SwapView*)this->parent();
   swapView->onUserSwapCancel();
   closeBPView();
}

void BPView::showBPData()
{
    int SOH=100;
    int32_t Temp = 25;
    this->ui->lblBatSN->setText(this->cab->getBp()->getSerialNumber());
    if(this->cab->getBp()->getSoc() < 25){
        this->ui->labelSOC->setStyleSheet("color: rgb(141, 198, 255);"
                                          "background: transparent;"
                                          "background-image:url(:/UI/img/SELEX SBS icon/Group -1.png);"
                                          "background-repeat: no-repeat;"
                                          "text-decoration: none;");
    }
    else if(this->cab->getBp()->getSoc() <50){
        this->ui->labelSOC->setStyleSheet("color: rgb(141, 198, 255);"
                                          "background: transparent;"
                                          "background-image:url(:/UI/img/SELEX SBS icon/Group -35.png);"
                                          "background-repeat: no-repeat;"
                                          "text-decoration: none;");
    }
    else{
        this->ui->labelSOC->setStyleSheet("color: rgb(141, 198, 255);"
                                          "background: transparent;"
                                          "background-image:url(:/UI/img/SELEX SBS icon/Group -2.png);"
                                          "background-repeat: no-repeat;"
                                          "text-decoration: none;");
    }
    this->ui->labelCabinNum->setText(QString::number(this->cab->getId()+1));
    this->ui->labelSOC->setText(QString::number(this->cab->getBp()->getSoc()));
    this->ui->lblSOH->setText(QString::number(SOH));
    this->ui->lblTemp->setText(QString::number(Temp));
    this->ui->lblCycles->setText(QString::number(this->cab->getBp()->getVoltage()));
}

void BPView::closeBPView()
{
    userActionTimeout->stop();
    close();
}

void BPView::on_btnSwap_pressed()
{
    this->ui->btnSwap->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 372.png);"
                                      "background-repeat: no-repeat;"
                                      "");
}

void BPView::on_btnSwap_released()
{
    this->ui->btnSwap->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 371.png);"
                                      "background-repeat: no-repeat;"
                                      "");
}

void BPView::on_btnCancel_pressed()
{
    this->ui->btnCancel->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 373.png);"
                                      "background-repeat: no-repeat;"
                                      "");
}

void BPView::on_btnCancel_released()
{
    this->ui->btnCancel->setStyleSheet("background: transparent;"
                                      "background-image:url(:/UI/img/SELEX SBS icon/Group 370.png);"
                                      "background-repeat: no-repeat;"
                                      "");
}
