#include "continue.h"
#include "ui_continue.h"
#include "QTimer"
Continue::Continue(const CabinetCom* cab, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Continue)
{
    ui->setupUi(this);
    this->cab=(CabinetCom*)cab;
    continueActionTimeout = new QTimer(this);
    connect(this->cab,&CabinetCom::onBSSLostConnect,this,&Continue::closeContinueView);
    connect(continueActionTimeout,&QTimer::timeout,this,&Continue::on_actionTimeout);
    connect(this, &Continue::destroyed, continueActionTimeout , &QTimer::deleteLater);
    continueActionTimeout->start(10000);
}

Continue::~Continue()
{
    delete ui;
}


void Continue::on_btnContinue_clicked()
{

    this->cab->getSwapRecord()->setState(SWAP_RECORD_ST_SELECT_EMPTY_CAB);
    this->cab->setSwappingFlag(true);
    SwapView* swapView = new SwapView(cab);
    swapView->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    swapView->showFullScreen();
    closeContinueView();
}

void Continue::on_btnCancel_clicked()
{
    //finish
    closeContinueView();
}

void Continue::on_actionTimeout(){
    //finish
    closeContinueView();
}

void Continue::closeContinueView(){
    continueActionTimeout->stop();
    close();
}


void Continue::on_btnContinue_pressed()
{
    this->ui->btnContinue->setStyleSheet("background: transparent;"
                                       "background-image:url(:/UI/img/SELEX SBS icon/Group 372.png);"
                                       "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}

void Continue::on_btnContinue_released()
{
    this->ui->btnContinue->setStyleSheet("background: transparent;"
                                       "background-image:url(:/UI/img/SELEX SBS icon/Group 371.png);"
                                       "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}



void Continue::on_btnCancel_pressed()
{
    this->ui->btnCancel->setStyleSheet("background: transparent;"
                                       "background-image:url(:/UI/img/SELEX SBS icon/Group 373.png);"
                                       "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}

void Continue::on_btnCancel_released()
{
    this->ui->btnCancel->setStyleSheet("background: transparent;"
                                       "background-image:url(:/UI/img/SELEX SBS icon/Group 370.png);"
                                       "background-repeat: no-repeat;"
                                       "background-position:left top;"
                                       "background-position:center;");
}
