/********************************************************************************
** Form generated from reading UI file 'continue.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTINUE_H
#define UI_CONTINUE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Continue
{
public:
    QWidget *centralwidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnCancel;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnContinue;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Continue)
    {
        if (Continue->objectName().isEmpty())
            Continue->setObjectName(QStringLiteral("Continue"));
        Continue->resize(800, 1280);
        Continue->setStyleSheet(QLatin1String("#centralwidget{\n"
"border-image: url(:/UI/img/Custom Size - 9.png);\n"
"}"));
        centralwidget = new QWidget(Continue);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(50, 920, 701, 151));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        btnCancel = new QPushButton(horizontalLayoutWidget);
        btnCancel->setObjectName(QStringLiteral("btnCancel"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnCancel->sizePolicy().hasHeightForWidth());
        btnCancel->setSizePolicy(sizePolicy);
        btnCancel->setStyleSheet(QLatin1String("background: transparent;\n"
"background-image: url(:/UI/img/SELEX SBS icon/Group 370.png);\n"
"background-repeat: no-repeat;\n"
"text-decoration: none;\n"
"background-position:left top;\n"
"background-position: center;"));

        horizontalLayout->addWidget(btnCancel);

        horizontalSpacer = new QSpacerItem(70, 19, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnContinue = new QPushButton(horizontalLayoutWidget);
        btnContinue->setObjectName(QStringLiteral("btnContinue"));
        sizePolicy.setHeightForWidth(btnContinue->sizePolicy().hasHeightForWidth());
        btnContinue->setSizePolicy(sizePolicy);
        btnContinue->setStyleSheet(QLatin1String("background: transparent;\n"
"background-image: url(:/UI/img/SELEX SBS icon/Group 371.png);\n"
"background-repeat: no-repeat;\n"
"text-decoration: none;\n"
"background-position:left top;\n"
"background-position: center;"));

        horizontalLayout->addWidget(btnContinue);

        Continue->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(Continue);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        Continue->setStatusBar(statusbar);

        retranslateUi(Continue);

        QMetaObject::connectSlotsByName(Continue);
    } // setupUi

    void retranslateUi(QMainWindow *Continue)
    {
        Continue->setWindowTitle(QApplication::translate("Continue", "MainWindow", Q_NULLPTR));
        btnCancel->setText(QString());
        btnContinue->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Continue: public Ui_Continue {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTINUE_H
